const BACKEND_BASE_URL = "http://localhost:5000";
// main.js

/*=============== SHOW MENU ===============*/
const navMenu = document.getElementById("nav-menu"),
  navToggle = document.getElementById("nav-toggle"),
  navClose = document.getElementById("nav-close");

/*===== MENU SHOW =====*/
/* Validate if constant exists */
if (navToggle) {
  navToggle.addEventListener("click", () => {
    navMenu.classList.add("show-menu");
  });
}

/*===== MENU HIDDEN =====*/
/* Validate if constant exists */
if (navClose) {
  navClose.addEventListener("click", () => {
    navMenu.classList.remove("show-menu");
  });
}

/*=============== REMOVE MENU MOBILE ===============*/
const navLink = document.querySelectorAll(".nav__link");

const linkAction = () => {
  const navMenu = document.getElementById("nav-menu");
  // When we click on each nav__link, we remove the show-menu class
  navMenu.classList.remove("show-menu");
};
navLink.forEach((n) => n.addEventListener("click", linkAction));

/*=============== CHANGE BACKGROUND HEADER ===============*/
const scrollHeader = () => {
  const header = document.getElementById("header");
  // When the scroll is greater than 50 viewport height, add the scroll-header class to the header tag
  this.scrollY >= 50
    ? header.classList.add("scroll-header")
    : header.classList.remove("scroll-header");
};
window.addEventListener("scroll", scrollHeader);

/*=============== TESTIMONIAL SWIPER ===============*/
let testimonialSwiper = new Swiper(".testimonial-swiper", {
  spaceBetween: 30,
  loop: "true",

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

/*=============== NEW SWIPER ===============*/
let newSwiper = new Swiper(".new-swiper", {
  spaceBetween: 24,
  loop: "true",

  breakpoints: {
    576: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    },
    1024: {
      slidesPerView: 4,
    },
  },
});

/*=============== SCROLL SECTIONS ACTIVE LINK ===============*/
const sections = document.querySelectorAll("section[id]");

const scrollActive = () => {
  const scrollY = window.pageYOffset;

  sections.forEach((current) => {
    const sectionHeight = current.offsetHeight,
      sectionTop = current.offsetTop - 58,
      sectionId = current.getAttribute("id"),
      sectionsClass = document.querySelector(
        ".nav__menu a[href*=" + sectionId + "]"
      );

    if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
      sectionsClass.classList.add("active-link");
    } else {
      sectionsClass.classList.remove("active-link");
    }
  });
};
window.addEventListener("scroll", scrollActive);

/*=============== SHOW SCROLL UP ===============*/
const scrollUp = () => {
  const scrollUp = document.getElementById("scroll-up");
  // When the scroll is higher than 350 viewport height, add the show-scroll class to the a tag with the scrollup class
  this.scrollY >= 350
    ? scrollUp.classList.add("show-scroll")
    : scrollUp.classList.remove("show-scroll");
};
window.addEventListener("scroll", scrollUp);

/*=============== SHOW CART ===============*/
const cart = document.getElementById("cart"),
  cartShop = document.getElementById("cart-shop"),
  cartClose = document.getElementById("cart-close");

/*===== CART SHOW =====*/
/* Validate if constant exists */
if (cartShop) {
  cartShop.addEventListener("click", () => {
    cart.classList.add("show-cart");
  });
}

/*===== CART HIDDEN =====*/
/* Validate if constant exists */
if (cartClose) {
  cartClose.addEventListener("click", () => {
    cart.classList.remove("show-cart");
  });
}

/*=============== DARK LIGHT THEME ===============*/
const themeButton = document.getElementById("theme-button");
const darkTheme = "dark-theme";
const iconTheme = "bx-sun";

// Previously selected topic (if user selected)
const selectedTheme = localStorage.getItem("selected-theme");
const selectedIcon = localStorage.getItem("selected-icon");

// We obtain the current theme that the interface has by validating the dark-theme class
const getCurrentTheme = () =>
  document.body.classList.contains(darkTheme) ? "dark" : "light";
const getCurrentIcon = () =>
  themeButton.classList.contains(iconTheme) ? "bx bx-moon" : "bx bx-sun";

// We validate if the user previously chose a topic
if (selectedTheme) {
  // If the validation is fulfilled, we ask what the issue was to know if we activated or deactivated the dark
  document.body.classList[selectedTheme === "dark" ? "add" : "remove"](
    darkTheme
  );
  themeButton.classList[selectedIcon === "bx bx-moon" ? "add" : "remove"](
    iconTheme
  );
}

// Activate / deactivate the theme manually with the button
themeButton.addEventListener("click", () => {
  // Add or remove the dark / icon theme
  document.body.classList.toggle(darkTheme);
  themeButton.classList.toggle(iconTheme);
  // We save the theme and the current icon that the user chose
  localStorage.setItem("selected-theme", getCurrentTheme());
  localStorage.setItem("selected-icon", getCurrentIcon());
});

let cardWidth = 8.56; //in cm

let zoll = 0;

function fetchImagesData() {
  // Make a GET request to the server
  fetch(BACKEND_BASE_URL + "/images")
    .then((response) => {
      // Check if the response was successful
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      // Parse the JSON response
      return response.json();
    })
    .then((data) => {
      // Log the response data to the console
      console.log("Images Data:", data);
      // You can further process the data here
    })
    .catch((error) => {
      // Log any errors to the console
      console.error("Error:", error);
    });
}

async function postRequest(pKey) {
  // Define the data to send in the request body
  const postData = {
    key: pKey,
  };

  try {
    const response = await fetch(BACKEND_BASE_URL + "/images", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postData),
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }

    const data = await response.json();
    const objectWidth = data.card.contour_dimensions[0];
    console.log("Width of the object:", objectWidth);
    return objectWidth;
  } catch (error) {
    // Log any errors to the console
    console.error("Error:", error);
    return 0; // Return a default value in case of error
  }
}

async function resize(selectedPic) {
  if (zoll == 0) {
    zoll = 15.6;
    alert("No screen size was entered. Default screen size is 15.6 inch.");
  }
  var i = document.getElementById(selectedPic.id);
  alert(i.id);
  var bildBreite = i.style.width; // bildbreite in Pixel
  var objectWidth = await postRequest(i.id); // Wait for postRequest to complete
  alert(objectWidth);
  bildBreite = bildBreite.replace("px", "");
  bildBreite = parseInt(bildBreite);
  a = 0; //höhe in cm
  b = 0; // breite in cm
  r = screen.width / screen.height; //ratio 16/9
  a = r * r; // c zum quadrat
  a = a + 1;
  a = Math.sqrt(a);
  a = zoll / a; // Höhe des Bildschirms in Zoll
  b = a * r; // Breite des Bildschirms in Zoll
  a = a * 2.54; // Höhe des Bildschirms in cm
  b = b * 2.54; // Breite des Bildschirms in cm
  //alert("Höhe in cm: " + a + ", Breite in cm: " + b);
  var ratio = bildBreite / objectWidth;
  alert("ratio is: " + ratio);
  bildBreite = screen.width * cardWidth;
  bildBreite = bildBreite / b;

  bildBreite = bildBreite * ratio;
  var newSize = bildBreite + "px";
  alert("picture will be changed to size: " + newSize);
  i.style.width = newSize;
}
//change object size

document.getElementById("screenWidth").textContent = screen.width;
let productWidth = new Array(1, 6, 12); // in cm
let productId = new Array(1, 2, 3); // ID number of product :D
let a = new Array(productWidth, productId);

let z = 0;

function screensizer() {
  z = document.eingabeFormular.eingabe.value; //zoll of the screen eingegeben
  zoll = z;
}

function umrechnen(p) {
  alert(z);
  var i = document.getElementById(p.id + "Pop");
  var pb = ""; //eigentliche Breite des Produktes (zB 6cm)
  var text = document.getElementById(p.id).id;
  const myArray = text.split("d");
  var s = myArray[1];
  var modal = document.getElementById("myModal" + s);
  modal.style.display = "block";
  var s2 = parseInt(s);
  //alert(s2);
  for (k = 0; k < productId.length; k++) {
    if (s2 == productId[k]) {
      pb = productWidth[k];
    }
  }
  var pb2 = parseInt(pb);
  //alert(pb2);
  var bb = 0; // bildbreite in Pixel
  a = 0; //höhe in cm
  b = 0; // breite in cm
  r = screen.width / screen.height; //ratio 16/9
  a = r * r; // c zum quadrat
  a = a + 1;
  a = Math.sqrt(a);
  a = z / a; // Höhe des Bildschirms in Zoll
  b = a * r; // Breite des Bildschirms in Zoll
  a = a * 2.54;
  b = b * 2.54;
  //alert("Höhe in cm: " + a + ", Breite in cm: " + b);
  bb = screen.width * pb2;
  bb = bb / b;
  var s = bb + "px";
  //alert(s);
  i.style.width = s;
}

function schließen(p) {
  var text = document.getElementById(p.id).id;
  const myArray = text.split("n");
  var s = myArray[1];
  var modal = document.getElementById("myModal" + s);
  modal.style.display = "none";
}
