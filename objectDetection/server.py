import json
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

# Your routes and other server code here

@app.route('/hello', methods=['GET'])
def hello():
    return 'Hello, World!'

# Load JSON data from files
with open('dimensions.json') as f:
    dimensions_data = json.load(f)

with open('image_info.json') as f:
    image_info_data = json.load(f)

# Route for /dimensions endpoint
@app.route('/dimensions', methods=['GET', 'POST'])
def dimensions():
    if request.method == 'GET':
        return jsonify(dimensions_data)
    elif request.method == 'POST':
        key = request.json.get('key')
        if key in dimensions_data:
            return jsonify({key: dimensions_data[key]})
        else:
            return jsonify({'error': 'Key not found'})

# Route for /images endpoint
@app.route('/images', methods=['GET', 'POST'])
def images():
    if request.method == 'GET':
        return jsonify(image_info_data)
    elif request.method == 'POST':
        key = request.json.get('key')
        if key in image_info_data:
            return jsonify({key: image_info_data[key]})
        else:
            return jsonify({'error': 'Key not found'})

if __name__ == '__main__':
    app.run(debug=True)
