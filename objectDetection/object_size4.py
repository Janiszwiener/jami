import os
import cv2
import json

# Load global variables
#with open('../globals.json', 'r') as f:
 #   globals_data = json.load(f)
    
# Specify the directory containing images
image_directory = r'./img'

#image_directory = '../' + globals_data['images_path']
# image_directory = Database-connect-API

# Get a list of files in the directory
image_files = [f for f in os.listdir(image_directory) if f.endswith(('.png', '.jpg', '.jpeg'))]

# Initialize a dictionary to store dimensions and image sizes
image_info = {}

#ID Count
id_counter = 1

# Loop through each file in the list
for file_name in image_files:
    # Construct full file path
    full_file_path = os.path.join(image_directory, file_name)

    # Load the image
    image = cv2.imread(full_file_path)
    if image is None:
        print(f"Failed to load image {full_file_path}. Please check the file path and permissions.")
        continue

    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Apply adaptive thresholding to create a binary image
    threshold = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)

    # Find contours in the binary image
    contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Check if any contours were found before finding the largest
    if contours:
        # Find the largest contour
        largest_contour = max(contours, key=cv2.contourArea)

        # Draw contours on the original image
        cv2.drawContours(image, [largest_contour], -1, (0, 255, 0), 2)

        # Get original image dimensions
        original_height, original_width = image.shape[:2]

        # Store the dimensions and original size in the dictionary
        image_info[os.path.splitext(file_name)[0]] = {
            'ID': id_counter,
            'contour_dimensions': (cv2.boundingRect(largest_contour)[2], cv2.boundingRect(largest_contour)[3]),
            'original_size': (original_width, original_height)
        }
        
        id_counter += 1

        # Display the image with contours
        cv2.imshow(f'Contours - {file_name}', image)
        

# Save image info dictionary to a JSON file
with open('objectDetection/image_info.json', 'w') as json_file:
    json.dump(image_info, json_file)

cv2.waitKey(0)  # Waits indefinitely until a key is pressed

cv2.destroyAllWindows()