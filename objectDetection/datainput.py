import os
import json
import cv2

# Load global variables
with open('../globals.json', 'r') as f:
    globals_data = json.load(f)
    
def find_image_file(directory, base_filename):
    extensions = ['png', 'jpg', 'jpeg']
    for ext in extensions:
        file_path = os.path.join(directory, f"{base_filename}.{ext}")
        if os.path.exists(file_path):
            return file_path
    return None

def display_image_with_text(image, text):
    """ Display the image with text overlay. """
    display_image = image.copy()
    cv2.putText(display_image, text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    cv2.imshow("Image", display_image)

def get_dimensions_via_gui(image):
    """ Use OpenCV window to get width and height input. """
    cv2.imshow('Image', image)
    cv2.waitKey(1)  # Display the window
    width = float(input("Enter the width in cm: "))
    height = float(input("Enter the height in cm: "))
    cv2.destroyAllWindows()
    return width, height

def update_image_dimensions(json_file_path, file_name):
    try:
        with open(json_file_path, 'r') as json_file:
            image_info = json.load(json_file)
    except FileNotFoundError:
        print("JSON file not found. Please check the file path.")
        return

    full_file_path = find_image_file('../' + globals_data['images_path']+ '/', file_name)
    if full_file_path is None:
        print("Image file not found. Please check the file name and extension.")
        return

    image = cv2.imread(full_file_path)
    if image is None:
        print("Failed to load the image. Please check the file path.")
        return

    width_cm, height_cm = get_dimensions_via_gui(image)
    image_info[file_name]['object_dimensions_cm'] = (width_cm, height_cm)

    with open(json_file_path, 'w') as json_file:
        json.dump(image_info, json_file)
    print("Dimensions updated successfully. Exported to database!")

def main():
    json_file_path = 'image_info.json'
    file_name = input("Enter the image file name without extension: ")
    update_image_dimensions(json_file_path, file_name)

if __name__ == "__main__":
    main()