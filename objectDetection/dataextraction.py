import json

def load_image_data(json_file_path):
    """ Load the image info from the JSON file. """
    try:
        with open(json_file_path, 'r') as file:
            image_info = json.load(file)
    except FileNotFoundError:
        print("File not found. Please check the file path.")
        return None
    except json.JSONDecodeError:
        print("Error decoding JSON. Check file content.")
        return None
    return image_info

def print_image_details(image_info, file_name):
    """ Print details for the specified image. """
    if image_info is not None and file_name in image_info:
        details = image_info[file_name]
        print(f"Details for {file_name}:")
        print(f"  ID: {details['ID']}")  # Print the ID
        print(f"  Contour Dimensions: {details['contour_dimensions'][0]} x {details['contour_dimensions'][1]} pixels")
        print(f"  Original Size: {details['original_size'][0]} x {details['original_size'][1]} pixels")
        if 'object_dimensions_cm' in details:
            print(f"  Object Dimensions: {details['object_dimensions_cm'][0]} cm x {details['object_dimensions_cm'][1]} cm")
        else:
            print("  Object dimensions in cm not available.")
    else:
        print("Image file not found in the database or no data to display.")

def main():
    json_file_path = 'image_info.json'  # Path to the JSON file containing image information
    file_name = input("Enter the image file name without extension: ")  # User inputs the file name without extension
    image_info = load_image_data(json_file_path)
    print_image_details(image_info, file_name)

if __name__ == "__main__":
    main()
